#ifndef IMAGE_HPP_
#define IMAGE_HPP_

#include <fstream>
#include "shape.hpp"

namespace Drawing
{

// Przystosowa� klas� image do wzorca prototype
class Image : public ShapeBase
{
public:
	Image(int x = 0, int y = 0, const std::string& path = "default.img") : ShapeBase(x, y), path_(path), buffer_(NULL), size_(0)
	{
		load_to_buffer();
	}

	~Image()
	{
		delete [] buffer_;
	}

	std::string path() const
	{
		return path_;
	}

	void set_path(const std::string& path)
	{
		delete [] buffer_;
		path_ = path;
		load_to_buffer();
	}

	virtual void draw() const
	{
		std::cout << "Drawing an image at: " << point() << " content: " << buffer_ << std::endl;
	}

protected:
	void load_to_buffer()
	{
		std::cout << "Loading a file " << path_ << "...\n";

		std::ifstream fin(path_.c_str(), std::ios_base::binary);

		if (!fin.is_open())
			throw std::runtime_error("File not found...");

		// odczyt dlugosci pliku
		fin.seekg (0, std::ios::end);
		int length_of_file = static_cast<int>(fin.tellg());

		if (length_of_file == -1)
			throw std::runtime_error("Input stream error");

		fin.seekg (0, std::ios::beg);

		// wczytanie bufora
		buffer_ = new char[length_of_file + 1];
		fin.read(buffer_, length_of_file);
		buffer_[length_of_file] = '\0';
		size_ = length_of_file + 1;
	}

private:
	std::string path_;
	char* buffer_;
	size_t size_;
};

}

#endif /* IMAGE_HPP_ */
