#ifndef PROTOTYPE_HPP_
#define PROTOTYPE_HPP_

#include <iostream>
#include <string>

// "Prototype" 

class Prototype
{
	std::string id_;
public:
	Prototype(const std::string& id) : id_(id)
	{
	}
	
	std::string get_id() const
	{
		return id_;
	}
	
	virtual Prototype* clone() = 0;
	
	virtual ~Prototype()
	{
	}
};


// "ConcretePrototype1"
class ConcretePrototype1 : public Prototype
{
public:
	ConcretePrototype1(const std::string& id) : Prototype(id)
	{
	}
	
	ConcretePrototype1* clone()
	{
		return new ConcretePrototype1(*this);
	}
};


// "ConcretePrototype1"
class ConcretePrototype2 : public Prototype
{
public:
	ConcretePrototype2(const std::string& id) : Prototype(id)
	{
	}
	
	ConcretePrototype2* clone()
	{
		return new ConcretePrototype2(*this);
	}
};

#endif /*PROTOTYPE_HPP_*/
