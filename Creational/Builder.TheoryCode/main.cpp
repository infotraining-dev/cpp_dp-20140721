#include <iostream>
#include <memory>
#include "builder.hpp"

using namespace std;

int main()
{
    // Create director and builders
    Director director;

    auto_ptr<Builder> b1(new ConcreteBuilder1());
    auto_ptr<Builder> b2(new ConcreteBuilder2());

    // Construct two products
    cout << "Building with ConcreteBuilder1:\n";
    director.construct(*b1);
    auto_ptr<Product> p1(b1->get_result());
    p1->show();

    cout << "\n\nBuilding with ConcreteBuilder2:\n";
    director.construct(*b2);
    auto_ptr<Product> p2(b2->get_result());
    p2->show();
}
