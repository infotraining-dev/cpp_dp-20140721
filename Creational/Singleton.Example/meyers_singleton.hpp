#ifndef MEYERS_SINGLETON_HPP_
#define MEYERS_SINGLETON_HPP_

namespace Meyers
{

// singleton Meyersa
template <typename T>
class SingletonHolder
{
public:
	static T& instance()
	{
		static T instance_;

		return instance_;
	}
private:
	SingletonHolder();
	~SingletonHolder();
	SingletonHolder(const SingletonHolder&);
	SingletonHolder& operator=(const SingletonHolder&);
};

}
#endif /* MEYERS_SINGLETON_HPP_ */
