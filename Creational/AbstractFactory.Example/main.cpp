#include "game.hpp"

using namespace Game;

int main()
{
	GameApp game;

    GameLevel level = DIE_HARD;

	game.select_level(level);
	game.init_game(12);
	game.test_monsters();
}
