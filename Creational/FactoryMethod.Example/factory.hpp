#ifndef FACTORY_HPP_
#define FACTORY_HPP_

#include "shape.hpp"
#include <map>
#include <stdexcept>
#include <boost/function.hpp>

namespace GenericFactory
{

	template
	<
		typename AbstractProduct,
		typename IdentifierType,
		typename CreatorType = boost::function<AbstractProduct* ()>
	>
	class Factory
	{
	private:
		typedef std::map<IdentifierType, CreatorType> CreatorsMapType;
	public:
		bool register_creator(const IdentifierType& id, CreatorType creator)
		{
			return creators_.insert(std::make_pair(id, creator)).second;
		}

		bool unregister_creator(const IdentifierType& id)
		{
			return creators_.erase(id) == 1;
		}

		AbstractProduct* create_object(const IdentifierType& id)
		{
			typename CreatorsMapType::const_iterator it = creators_.find(id);
			if (it == creators_.end())
				throw std::runtime_error((std::string("Unknown Type ID: ") + id));

			return (it->second)();
		}

	private:
		CreatorsMapType creators_;
	};
}

#endif /* FACTORY_HPP_ */
