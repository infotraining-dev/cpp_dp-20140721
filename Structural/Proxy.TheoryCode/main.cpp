#include "proxy.hpp"

class Client
{
	Subject* subject_;
	Client(const Client&);
	Client& operator=(const Client&);
public:
	Client(Subject* subject) : subject_(subject)
	{
	}

	~Client()
	{
		delete subject_;
	}

	void use()
	{
		subject_->request();
	}
};

int main()
{
	Client c(new Proxy());

	std::cout << std::endl;

	c.use();
	c.use();
}
