#ifndef PROXY_HPP_
#define PROXY_HPP_

#include <iostream>
#include <string>
#include <mutex>

// "Subject" 
class Subject
{
public:
    virtual void request() const = 0;
    virtual ~Subject() {}
};

// "RealSubject"
class RealSubject : public Subject
{
public:
    RealSubject()
    {
        std::cout << "RealSubject's creation" << std::endl;
    }

    ~RealSubject()
    {
        std::cout << "RealSubject's clean-up" << std::endl;
    }

    void request() const
    {
        std::cout << "Called RealSubject.request()" << std::endl;
    }
};

class SynchronizedSubject : public Subject
{
    RealSubject real_subject_;
    mutable std::mutex mtx_;
public:
    void request() const
    {
        std::lock_guard<std::mutex> lk(mtx_);

        real_subject_.request();
    }
};

// "Proxy" 
class Proxy : public Subject
{
    mutable RealSubject* real_subject_;

    Proxy(const Proxy&);
    Proxy& operator=(const Proxy&);
public:
    Proxy() :
        real_subject_(0)
    {
        std::cout << "Proxy's creation" << std::endl;
    }

    ~Proxy()
    {
        delete real_subject_;
    }

    void request() const
    {
        // lazy initialization'
        if (real_subject_ == NULL)
        {
            real_subject_ = new RealSubject();
        }

        real_subject_->request();
    }
};

#endif /*PROXY_HPP_*/
