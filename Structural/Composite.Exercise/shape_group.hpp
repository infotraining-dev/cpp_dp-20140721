#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"

namespace Drawing
{

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
class ShapeGroup : public Shape
{
    std::vector<Shape*> shapes_;
public:
    ShapeGroup()
    {}

    ShapeGroup(const ShapeGroup& source)
    {
        std::transform(source.shapes_.begin(), source.shapes_.end(),
                       std::back_inserter(shapes_),
                       std::mem_fn(&Shape::clone));
    }

    void draw() const
    {
        for(auto s : shapes_)
            s->draw();
    }

    void move(int dx, int dy)
    {
        for(auto s : shapes_)
            s->move(dx, dy);
    }

    ShapeGroup* clone() const
    {
        return new ShapeGroup(*this);
    }

    void read(std::istream &in)
    {
        int count;

        in >> count;

        std::string type_identifier;

        for(int i = 0; i < count; ++i)
        {
            in >> type_identifier;
            Shape* shp = ShapeFactory::instance().create(type_identifier);
            shp->read(in);
            add(shp);
        }
    }

    void write(std::ostream &out)
    {
        out << "ShapeGroup " << shapes_.size() << std::endl;

        for(auto s : shapes_)
            s->write(out);
    }

    void add(Shape* shp)
    {
        shapes_.push_back(shp);
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
