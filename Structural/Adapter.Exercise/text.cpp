#include "text.hpp"
#include "clone_factory.hpp"

namespace
{
	// TODO - rejestracja prototypu Text
    using namespace Drawing;

    bool is_registered =
            ShapeFactory::instance()
            .register_shape("Text", new Text());

}
