#include "decorator.hpp"

int main()
{
	 // Create ConcreteComponent and two Decorators
     Component* d2 = new ConcreteDecoratorB(new ConcreteDecoratorA(new ConcreteComponent));

	 d2->operation();

	 std::cout << std::endl;

     delete d2;
}
