#ifndef COFFEEHELL_HPP_
#define COFFEEHELL_HPP_

#include <iostream>
#include <string>

class Coffee
{
protected:
	float price_;
	std::string description_;
public:
	Coffee(float price, const std::string& description) : price_(price), description_(description)
	{
	}

	virtual float get_total_price() const
	{
		return price_;
	}

	virtual std::string get_description() const
	{
		return description_;
	}

	virtual void prepare() = 0;


	virtual ~Coffee() {}
};

class Espresso : public Coffee
{
public:
	Espresso(float price = 4.0, const std::string& description = "Espresso")
		: Coffee(price, description)
	{
	}

	void prepare()
	{
		std::cout << "Making a perfect espresso: 7 g, 15 bar and 24 sec.\n";
	}
};

class Cappuccino : public Coffee
{
public:
	Cappuccino(float price = 6.0, const std::string& description = "Cappuccino")
		: Coffee(price, description)
	{
	}

	void prepare()
	{
		std::cout << "Making a perfect cappuccino.\n";
	}
};

class Latte : public Coffee
{
public:
	Latte(float price = 8.0, const std::string& description = "Latte")
		: Coffee(price, description)
	{
	}

	void prepare()
	{
		std::cout << "Making a perfect latte.\n";
	}
};


// TO DO: Dodatki: cena - Whipped: 2.5, Whiskey: 6.0, ExtraEspresso: 4.0

// TO DO: Utworzy� klas� CoffeeDecorator i klasy konkretnych dodatk�w.
//        Utworzy� espresso udekorowane dodatkami, obliczy� cen�, wy�wietli� opis i przygotowa� nap�j.

class CoffeeDecorator : public Coffee
{
    Coffee* coffee_;

    CoffeeDecorator(const CoffeeDecorator&);
    CoffeeDecorator& operator=(const CoffeeDecorator&);
public:
    CoffeeDecorator(Coffee* coffee, float price, const std::string& description)
        : Coffee(price, description), coffee_(coffee)
    {
    }

    ~CoffeeDecorator()
    {
        delete coffee_;
    }

    float get_total_price() const
    {
        return coffee_->get_total_price() + price_;
    }

    std::string get_description() const
    {
        return coffee_->get_description() + " + " + description_;
    }

    void prepare()
    {
        coffee_->prepare();
    }
};

class Whisky : public CoffeeDecorator
{
public:
    Whisky(Coffee* coffee) : CoffeeDecorator(coffee, 6.0, "Whisky")
    {
    }

    void prepare()
    {
        CoffeeDecorator::prepare();
        std::cout << "Pouring whisky: 50cl\n";
    }
};

class Whipped : public CoffeeDecorator
{
public:
    Whipped(Coffee* coffee) : CoffeeDecorator(coffee, 2.5, "Whipped Cream")
    {
    }

    void prepare()
    {
        CoffeeDecorator::prepare();
        std::cout << "Adding a whipped cream\n";
    }
};

class ExtraEspresso : public CoffeeDecorator
{
    Espresso espresso_;
public:
    ExtraEspresso(Coffee* coffee) : CoffeeDecorator(coffee, 4.0, "Extra Espresso")
    {
    }

    void prepare()
    {
        CoffeeDecorator::prepare();
        espresso_.prepare();
    }
};

#endif /*COFFEEHELL_HPP_*/
