#include "coffeehell.hpp"
#include <memory>

class CoffeeBuilder
{
    std::unique_ptr<Coffee> coffee_;
public:
    template <typename T>
    CoffeeBuilder& create_base()
    {
        coffee_.reset(new T());
        return *this;
    }

    template <typename T>
    CoffeeBuilder& add()
    {
        Coffee* temp = coffee_.release();
        coffee_.reset(new T(temp));

        return *this;
    }

    std::unique_ptr<Coffee> get_coffee()
    {
        return std::move(coffee_);
    }
};


int main()
{
//    Coffee* cf = new Whipped(
//                    new Whisky(
//                        new Whisky(
//                            new Whisky(
//                                new ExtraEspresso(
//                                    new Espresso())))));

    CoffeeBuilder cb;
    cb.create_base<Espresso>();
    cb.add<Whisky>().add<Whisky>().add<Whipped>();
    std::unique_ptr<Coffee> cf = cb.get_coffee();

    std::cout << "Description: " << cf->get_description()
              << "; Price: " << cf->get_total_price() << std::endl;

    cf->prepare();
}
