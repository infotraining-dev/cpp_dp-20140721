#ifndef APPLICATION_HPP_
#define APPLICATION_HPP_

#include "document.hpp"
#include "command.hpp"
#include <map>

class MenuItem
{
	std::string caption_;
	Command* cmd_;

public:
	MenuItem(const std::string& caption, Command* cmd)
		: caption_(caption), cmd_(cmd)
	{
	}

	void on_enter()
	{
        std::cout << "Wykonano komende " << caption_ << "..." << std::endl;
		cmd_->execute();
	}

	~MenuItem()
	{
	}
};

class Application
{
	Document* document_;
public:
	std::map<std::string, MenuItem*> menu_;

public:
	Application(Document* document) : document_(document)
	{
	}

	void add_menu(const std::string& caption, MenuItem* item)
	{
		menu_.insert(std::make_pair(caption, item));
	}

	bool execute_action(const std::string& action_name)
	{
		if (menu_.count(action_name))
		{
			menu_[action_name]->on_enter();

			return true;
		}

		return false;
	}

	~Application()
	{
	}
};

#endif /* APPLICATION_HPP_ */
