#include <iostream>
#include "chain.hpp"

using namespace std;

int main()
{
	// Setup Chain of Responsibility
	Handler* h1 = new ConcreteHandler1();
	Handler* h2 = new ConcreteHandler2();
	Handler* h3 = new ConcreteHandler3();
	h1->set_successor(h2);
	h2->set_successor(h3);

	// Generate and process request
    int requests[8] = {2, 5, 14, 292, 18, 3, 27, 20};

	for(int* p = requests; p != requests + 8; ++p)
    {
        h1->handle(*p);
    }

	delete h3;
	delete h2;
	delete h1;
}
