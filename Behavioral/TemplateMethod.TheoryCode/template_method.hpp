#ifndef TEMPLATE_METHOD_HPP_
#define TEMPLATE_METHOD_HPP_

#include <iostream>
#include <string>
#include <memory>

class Service
{
public:
    virtual void run()
    {
        std::cout << "Service::run()" << std::endl;
    }

    virtual ~Service()
    {}
};

class BetterService : public Service
{
public:
    virtual void run()
    {
        std::cout << "BetterService::run()" << std::endl;
    }
};

// "AbstractClass"
class AbstractClass
{
protected:
	virtual void primitive_operation_1() = 0;
	virtual void primitive_operation_2() = 0;

    virtual std::unique_ptr<Service> create_service() const
    {
        return std::unique_ptr<Service>(new Service);
    }

    virtual void hook_method() {}
public:
	void template_method()
	{
		primitive_operation_1();
        std::unique_ptr<Service> srv = create_service();
        srv->run();
		primitive_operation_2();
        hook_method();
		std::cout << std::endl;
	}
	
	virtual ~AbstractClass() {}
};

// "ConcreteClass"
class ConcreteClassA : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation2()" << std::endl;
	}
};

// "ConcreteClass"
class ConcreteClassB : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation2()" << std::endl;
	}

    std::unique_ptr<Service> create_service() const
    {
        return std::unique_ptr<Service>(new BetterService);
    }

    void hook_method()
    {
        std::cout << "Logging..." << std::endl;
    }
};

#endif /*TEMPLATE_METHOD_HPP_*/
